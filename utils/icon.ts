import AppleMusicIcon from "../svg/apple_music.svg";
import FacebookIcon from "../svg/facebook.svg";
import InstagramIcon from "../svg/instagram.svg";
import LinkedInIcon from "../svg/linkedin.svg";
import SnapchatIcon from "../svg/snapchat.svg";
import SoundCloudIcon from "../svg/soundcloud.svg";
import SpotifyIcon from "../svg/spotify.svg";
import TiktokIcon from "../svg/tiktok.svg";
import TwitterIcon from "../svg/twitter.svg";
import YouTubeIcon from "../svg/youtube.svg";
import AppIcon from "../svg/icon.svg";

const nameToIcon: { [key: string]: any } = {
  apple_music: AppleMusicIcon,
  facebook: FacebookIcon,
  instagram: InstagramIcon,
  linkedin: LinkedInIcon,
  snapchat: SnapchatIcon,
  soundcloud: SoundCloudIcon,
  spotify: SpotifyIcon,
  tiktok: TiktokIcon,
  twitter: TwitterIcon,
  youtube: YouTubeIcon,
  logo: AppIcon,
};

export default nameToIcon;
