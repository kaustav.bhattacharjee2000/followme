import React, { useState, useCallback } from "react";
import Image from "next/image";
import { AppContext, AppProps } from "next/app";
import "simplebar/dist/simplebar.min.css";
import SimpleBar from "simplebar-react";
import { signOut, getSession } from "next-auth/client";
import {
  Avatar,
  Container,
  Flex,
  Box,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  HStack,
  Stack,
  Text,
  Switch,
  Button,
  Spacer,
  Center,
  Icon,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Divider,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  List,
  ListItem,
  Input,
  Textarea,
  SimpleGrid,
  useClipboard,
  useBreakpointValue,
  createIcon,
  chakra,
  Stat,
  StatLabel,
  StatNumber,
  StatGroup,
  Square,
} from "@chakra-ui/react";

import {
  HamburgerIcon,
  ViewOffIcon,
  EditIcon,
  ChevronDownIcon,
  ChevronUpIcon,
  IconProps,
  CloseIcon,
} from "@chakra-ui/icons";
import {
  IoTrashOutline,
  IoImageOutline,
  IoBarChartOutline,
} from "react-icons/io5";
import { HiPencil } from "react-icons/hi";
import { FaSmile } from "react-icons/fa";
import { ComponentWithAs } from "@chakra-ui/system";
import { IconType } from "react-icons";
import { Doughnut } from "react-chartjs-2";
import equal from "fast-deep-equal";

import Layout from "../components/Layout";
import MobileView from "../components/MobileView";
import useWindowDimensions from "../hooks/useWindowDimentions";
import {
  UserInterface,
  SocialLinkType,
  CustomLinkType,
} from "../types/profile";
import prisma from "../utils/prisma";
import defaults from "../defaults";

// const data = require("../data/profile.json");
const socialLinksInfo = require("../data/socialLinks.json");

interface countTagProps {
  title: string;
  count?: number;
  color: string;
}

interface sectionCardProps {
  title: string;
  icon?: ComponentWithAs<"svg", IconProps> | IconType;
  children: React.ReactNode;
  noBg?: boolean;
}

interface subSectionProps {
  data: UserInterface;
  methods: any;
}

interface ChangesType {
  updateTheme: Record<string, any>;
  addCustomLinks: Record<string, any>[];
  updateCustomLinks: Record<string, any>[];
  deleteCustomLinks: Record<string, any>[];
  addSocialLinks: Record<string, any>[];
  updateSocialLinks: Record<string, any>[];
  deleteSocialLinks: Record<string, any>[];
  updateProfile: Record<string, any>;
}

const UserIcon = createIcon({
  displayName: "UserIcon",
  viewBox: "0 0 20 20",
  path: (
    <path
      fillRule="evenodd"
      d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
      clipRule="evenodd"
    />
  ),
});

const CircleIcon = createIcon({
  displayName: "CircleIcon",
  viewBox: "0 0 200 200",
  path: (
    <path
      fill="currentColor"
      d="M 100, 100 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0"
    />
  ),
});

const CountTag: React.FC<countTagProps> = ({ title, count, color }) => (
  <HStack>
    <CircleIcon w={3} h={3} color={color} />
    <Text>{`${title}: ${count || "-"}`}</Text>
  </HStack>
);

const ScrollContent: React.FC<React.ReactNode> = ({ children }) => {
  const { height } = useWindowDimensions();
  const marginTop = useBreakpointValue({ base: 120, sm: 65 }) || 0;

  return (
    <SimpleBar style={{ maxHeight: height - marginTop }}>{children}</SimpleBar>
  );
};

function SectionCard(props: sectionCardProps) {
  return (
    <Stack spacing={4}>
      <HStack spacing={4}>
        {props.icon && (
          <Box
            bg="white"
            paddingX="6px"
            rounded="lg"
            border="2px"
            borderColor="purple.500"
          >
            <Icon as={props.icon} color="purple.500" w={3} h={3} />
          </Box>
        )}
        <Text as="strong" fontSize={18}>
          {props.title}
        </Text>
      </HStack>
      <Box
        bg={props.noBg ? "transparent" : "white"}
        py="4"
        px="6"
        color="gray.600"
      >
        {props.children}
      </Box>
    </Stack>
  );
}

function CustomLink() {
  const [showExtra, setShowExtra] = useState("");
  const [thumbnail, setThumbnail] = useState<any>(null);

  const showThumbnail = () => {
    setShowExtra("thumbnail");
  };

  const showAnalytics = () => {
    setShowExtra("analytics");
  };

  const hideExtra = () => {
    setShowExtra("");
  };

  const Extra: React.FC<any> = ({ children, title, description }) => (
    <Box w="full" bg="gray.200" p="2" rounded="md">
      <Flex mb="2" justifyContent="space-between" alignItems="center">
        <Box></Box>
        <Text as="strong">{title}</Text>
        <CloseIcon cursor="pointer" onClick={hideExtra} />
      </Flex>
      <Stack spacing="2">
        {description && <Text textAlign="center">{description}</Text>}
        {children}
      </Stack>
    </Box>
  );

  return (
    <Flex
      // key={data.id}
      alignItems="center"
      bg="white"
      boxShadow="md"
      color="gray.500"
      py="4"
    >
      <Flex
        h="full"
        py="5"
        flexDir="column"
        justifyContent="space-between"
        mx={1}
      >
        <ChevronUpIcon w={6} h={6} />
        <ChevronDownIcon w={6} h={6} />
      </Flex>

      <Divider orientation="vertical" />

      <Stack flex="1" px="4" spacing="4">
        <Flex>
          <Box>
            <HStack>
              <Text as="strong">Title</Text>
              <HiPencil />
            </HStack>
            <HStack>
              <Text>Url</Text>
              <HiPencil />
            </HStack>
          </Box>
          <Spacer />
          <Switch colorScheme="green" />
        </Flex>

        <Flex>
          <HStack spacing={4}>
            <Icon
              cursor="pointer"
              as={IoImageOutline}
              w={5}
              h={5}
              onClick={showThumbnail}
            />
            <Icon
              cursor="pointer"
              as={IoBarChartOutline}
              w={5}
              h={5}
              onClick={showAnalytics}
            />
          </HStack>
          <Spacer />
          <Icon cursor="pointer" as={IoTrashOutline} w={5} h={5} />
        </Flex>

        {showExtra === "thumbnail" && (
          <Extra
            title="Add Thumbnail"
            description="Add a Thumbnail or Icon to this Link"
          >
            <Button w="full" colorScheme="purple">
              Set Thumbnail
            </Button>
          </Extra>
        )}

        {showExtra === "analytics" && (
          <Extra title="Link Analytics">
            <Text textAlign="center">This link has been clicked {2} times</Text>
          </Extra>
        )}
      </Stack>
    </Flex>
  );
}

function Statistics() {
  const roundOff = (value: number) =>
    Math.round((value + Number.EPSILON) * 100) / 100;

  const OverallStat: React.FC<any> = ({ title, value }) => {
    return (
      <Stat>
        <StatLabel>{title}</StatLabel>
        <StatNumber>{value}</StatNumber>
      </Stat>
    );
  };

  const DeviceStat: React.FC<any> = ({ title, views, clicks }) => {
    return (
      <Stack>
        <Text as="strong">{title}</Text>
        <HStack fontSize={{ base: "base", lg: "sm" }} color="gray.500">
          <Text>{views} views</Text>
          <Text>{clicks} clicks</Text>
          <Text>{clicks > 0 ? roundOff(views / clicks) : 0}% CTR</Text>
        </HStack>
      </Stack>
    );
  };

  return (
    <Stack w="full" spacing={4}>
      <StatGroup>
        <OverallStat title="Total views" value={5} />
        <OverallStat title="Total clicks" value={2} />
        <OverallStat title="Avg. CTR" value={`${2.5}%`} />
      </StatGroup>

      <Flex
        alignItems="center"
        flexDir={{ base: "column", lg: "row" }}
        w="full"
      >
        <Box flex="1">
          <Doughnut
            // redraw={true}
            id="device-overall"
            type="doughnut"
            options={{
              responsive: true,
              maintainAspectRatio: true,
            }}
            data={{
              labels: ["Mobile", "Desktop", "Tablet", "Others"],
              datasets: [
                {
                  data: [200, 100, 20, 5],
                  backgroundColor: [
                    "rgb(255, 99, 132)",
                    "rgb(54, 162, 235)",
                    "rgb(255, 205, 86)",
                    "rgb(88, 92, 97)",
                  ],
                  hoverOffset: 4,
                },
              ],
            }}
          />
        </Box>
        <Stack w={{ base: "full", lg: "auto" }} ml="4" mr="2">
          <DeviceStat title="Mobile" views={5} clicks={2} />
          <DeviceStat title="Desktop" views={5} clicks={2} />
          <DeviceStat title="Tablet" views={5} clicks={2} />
          <DeviceStat title="Other" views={5} clicks={2} />
        </Stack>
      </Flex>
    </Stack>
  );
}

function LinksSection({ data }: subSectionProps) {
  const shrinkedAnalyticsSpacing = useBreakpointValue({ base: 2, lg: 8 });

  return (
    <ScrollContent>
      <Flex flex="1" flexDir="column">
        <Accordion bg="white" allowToggle>
          <AccordionItem>
            <AccordionButton fontSize={15} py={4}>
              <HStack spacing={shrinkedAnalyticsSpacing} flex="1">
                <Text fontWeight="bold">Lifetime Analytics:</Text>
                <CountTag title="Views" count={5} color="green.300" />
                <CountTag title="Clicks" count={2} color="purple.400" />
              </HStack>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pt={4}>
              <Statistics />
            </AccordionPanel>
          </AccordionItem>
        </Accordion>

        <Box flex="1" p="8">
          <Button colorScheme="purple" w="100%" mb="8">
            Add New Link
          </Button>
          <List spacing="4">
            {[1, 2, 3].map((v, i) => (
              <ListItem key={`${v}_${i}`}>
                <CustomLink />
              </ListItem>
            ))}
          </List>
        </Box>
      </Flex>
    </ScrollContent>
  );
}

function AppearanceSection({ data }: subSectionProps) {
  const bgImage = (theme: any) =>
    theme.background.coverType === "gradient"
      ? theme.background.gradient
      : `url('${theme.background.image}')`;

  const DisplayButton: React.FC<any> = ({ name, theme }) => (
    <Box
      key={`Button-${name}`}
      w="full"
      h="10"
      border="2px"
      borderColor={theme.borderColor}
      bg={theme.bg}
      rounded={theme.rounded}
      cursor="pointer"
      style={
        theme.shadow && {
          boxShadow: `${theme.shadow.shadowPos} rgba(0, 0, 0, 0.36)`,
        }
      }
    >
      {"    "}
    </Box>
  );

  const DisplayTheme: React.FC<any> = ({ name, theme, onlyBg }) => (
    <Flex flexDir="column" key={name} h="64" cursor="pointer">
      <Center
        w="full"
        p="4"
        rounded="md"
        bgImg={bgImage(theme)}
        flex="1"
        border="1px"
        borderColor="gray.300"
      >
        <Stack w="full" spacing="2">
          {!onlyBg &&
            [1, 2, 3].map((_, index) => (
              <Box
                key={`Button-${name}-${index}`}
                w="full"
                h="6"
                border="2px"
                borderColor={theme.link.borderColor}
                color={theme.link.textColor}
                bg={theme.link.backgroundColor}
                rounded={theme.link.roundCorners}
                cursor="pointer"
                _hover={{
                  bg: theme.link.onHover.backgroundColor,
                  color: theme.link.onHover.textColor,
                }}
              >
                {"    "}
              </Box>
            ))}
        </Stack>
      </Center>
      <Text textAlign="center">{name}</Text>
    </Flex>
  );

  const buttons = {
    borders: ["none", "md", "full"],
    types: [
      {
        title: "Fill",
        bg: true,
      },
      {
        title: "Outline",
        bg: false,
      },
      {
        title: "Hard shadow",
        bg: false,
        shadow: {
          shadowPos: "4px 4px 0px 0px",
        },
      },
      {
        title: "Soft shadow",
        bg: false,
        shadow: {
          shadowPos: "4px 4px 8px 0px",
        },
      },
    ],
  };

  return (
    <ScrollContent>
      <Box px="4" py="8">
        <List spacing="8">
          <ListItem>
            <SectionCard title="Themes">
              {/* Custom themes  */}
              <SimpleGrid spacing={6} columns={{ base: 2, lg: 3 }}>
                {Object.entries(defaults.themes).map(([name, theme]) => (
                  <DisplayTheme name={name} theme={theme} />
                ))}
              </SimpleGrid>

              <Text fontWeight="bold" fontSize="18" mt="12">
                Fixed colors
              </Text>
              <SimpleGrid columns={{ base: 1, lg: 2 }} spacing={4} mt="4">
                <Stack>
                  <Text>Background color</Text>
                  <HStack spacing="4">
                    <Square rounded="md" size="40px" bg="purple.700"></Square>
                    <Input placeholder="#888888" size="md" />
                  </HStack>
                </Stack>

                <Stack>
                  <Text>Text color</Text>
                  <HStack spacing="4">
                    <Square rounded="md" size="40px" bg="purple.700"></Square>
                    <Input placeholder="#888888" size="md" />
                  </HStack>
                </Stack>
              </SimpleGrid>
            </SectionCard>
          </ListItem>

          <ListItem>
            <SectionCard title="Buttons">
              <Stack spacing="6">
                {buttons.types.map((type) => (
                  <Stack spacing="2">
                    <Text>{type.title}</Text>
                    <SimpleGrid spacing={6} columns={{ base: 2, lg: 3 }}>
                      {buttons.borders.map((border) => (
                        <DisplayButton
                          name={`${type.title}-${border}`}
                          theme={{
                            borderColor: type.bg
                              ? "transparent"
                              : "blackAlpha.500",
                            bg: type.bg ? "blackAlpha.500" : "transparent",
                            rounded: border,
                            shadow: type.shadow,
                          }}
                        />
                      ))}
                    </SimpleGrid>
                  </Stack>
                ))}
              </Stack>
            </SectionCard>
          </ListItem>
        </List>
      </Box>
    </ScrollContent>
  );
}

function SettingsSection({ data, methods }: subSectionProps) {
  const MAX_BIO_LENGTH = 80;
  const [bio, setBio] = useState("");

  const saveBio = useCallback((event) => {
    let typed = event.target.value;
    if (typed.length <= MAX_BIO_LENGTH) {
      setBio(typed);
    }
  }, []);

  const saveSocialLink = useCallback((event, type) => {
    let typedUrl = event.target.value;
    let existingUrl = getSocialLink(type);

    if (!equal(typedUrl, existingUrl)) {
      if (!typedUrl || typedUrl.length === 0) {
        console.log("!! DELETE !!");
        // Delete
        methods.makeChanges({
          ...data,
          socialLinks: data.socialLinks.filter((link) => link.type === type),
        });
      } else if (!existingUrl) {
        console.log("!! ADD !!");
        // Add
        methods.makeChanges({
          ...data,
          socialLinks: data.socialLinks.concat({ id: -1, type, url: typedUrl }),
        });
      } else {
        console.log("!! UPDATE !!");
        // Update
        methods.makeChanges({
          ...data,
          socialLinks: data.socialLinks.map((link) =>
            link.type === type ? { ...link, url: typedUrl } : link
          ),
        });
      }
    }
  }, []);

  const getSocialLink = (type: string) => {
    let existingData = data.socialLinks.find(
      (obj: SocialLinkType) => obj.type === type
    );
    if (existingData) {
      return existingData.url;
    }
  };

  return (
    <ScrollContent>
      <Box px="4" py="8">
        <List spacing="8">
          <ListItem>
            <SectionCard title="Profile" icon={EditIcon}>
              <Stack spacing="4" mb="4">
                <HStack spacing="4">
                  {/* <UserIcon h={24} w={24} fill="green.400" cursor="pointer" /> */}
                  <Avatar
                    h={24}
                    w={24}
                    bg="green.500"
                    src={data.avatar}
                    rounded="full"
                    objectFit="contain"
                    cursor="pointer"
                  />

                  <SimpleGrid
                    flex="1"
                    columns={{ base: 1, md: 2 }}
                    spacing={{ base: 2, md: 4 }}
                  >
                    <Button w="full" colorScheme="purple">
                      Pick an image
                    </Button>
                    <Button w="full" colorScheme="gray">
                      Remove
                    </Button>
                  </SimpleGrid>
                </HStack>

                <Stack spacing="1">
                  <Text fontSize={14}>Profile Title</Text>
                  <Input placeholder={`@${data.username}`} />
                </Stack>

                <Stack spacing="1">
                  <Text fontSize={14}>Bio</Text>
                  <Textarea
                    value={bio}
                    onChange={saveBio}
                    placeholder="Enter a bio description to appear on your FollowMe"
                  />
                  <Text fontSize={12} color="gray.800" align="right">
                    {bio.length}/{MAX_BIO_LENGTH}
                  </Text>
                </Stack>
              </Stack>
            </SectionCard>
          </ListItem>

          <ListItem>
            <SectionCard title="Sensitive Material" icon={ViewOffIcon}>
              <Flex>
                <Text flex="1" fontSize="sm">
                  Visitors to your Linktree will see a Sensitive Content warning
                  before being able to view your profile.
                </Text>
                <Switch
                  colorScheme="green"
                  ml="2"
                  isChecked={data.matureContent}
                />
              </Flex>
            </SectionCard>
          </ListItem>

          <ListItem>
            <SectionCard title="Social Links" icon={FaSmile}>
              <List spacing="8">
                {socialLinksInfo.map((eachLink: any) => (
                  <ListItem>
                    <Stack spacing="1">
                      <Text fontSize={14}>{eachLink.title}</Text>
                      <Input
                        placeholder={eachLink.placeholder}
                        value={getSocialLink(eachLink.type)}
                        onBlur={(event) => saveSocialLink(event, eachLink.type)}
                      />
                    </Stack>
                  </ListItem>
                ))}
              </List>
            </SectionCard>
          </ListItem>
        </List>
      </Box>
    </ScrollContent>
  );
}

function AdminPage(props: { data: UserInterface }) {
  const profileURL = `${process.env.NEXT_PUBLIC_BASE_URL}/${props.data.username}`;
  const { hasCopied, onCopy } = useClipboard(profileURL);
  const emptyChanges: ChangesType = {
    updateTheme: {},
    addCustomLinks: [],
    updateCustomLinks: [],
    deleteCustomLinks: [],
    addSocialLinks: [],
    deleteSocialLinks: [],
    updateSocialLinks: [],
    updateProfile: {},
  };

  const [originalData, setOriginalData] = useState(props.data);
  const [changeData, setChangeData] = useState(props.data);
  const [changes, setChanges] = useState(emptyChanges);

  const updateTheme = (newTheme: any) => {
    // setChanges((prev) => ({ ...prev, updateTheme: newTheme }));
    setChangeData((prev) => ({ ...prev, theme: newTheme }));
  };

  const updateCustomLink = (updatedLink: CustomLinkType) => {
    // setChanges((prev) => ({
    //   ...prev,
    //   updateCustomLinks: prev.updateCustomLinks.map((link) => {
    //     if (link.id === updatedLink.id) {
    //       return updatedLink;
    //     }
    //     return link;
    //   }),
    // }));

    setChangeData((prev) => ({
      ...prev,
      customLinks: prev.customLinks.map((link) => {
        if (link.id === updatedLink.id) {
          return updatedLink;
        }
        return link;
      }),
    }));
  };

  const deleteCustomLink = (deleteLink: CustomLinkType) => {
    // setChanges((prev) => ({
    //   ...prev,
    //   deleteCustomLinks: prev.deleteCustomLinks.filter(
    //     (link: any) => link.id === deleteLink.id
    //   ),
    // }));

    setChangeData((prev) => ({
      ...prev,
      customLinks: prev.customLinks.filter((link) => link.id === deleteLink.id),
    }));
  };

  const addCustomLink = (newLink: CustomLinkType) => {
    // setChanges((prev) => ({
    //   ...prev,
    //   addCustomLinks: prev.addCustomLinks.concat(newLink),
    // }));

    setChangeData((prev) => ({
      ...prev,
      customLinks: prev.customLinks.concat(newLink),
    }));
  };

  const updateProfile = (updatedProfile: Record<string, any>) => {
    // setChanges((prev) => ({ ...prev, updateProfile: updatedProfile }));
    setChangeData((prev) => ({ ...prev, ...updatedProfile }));
  };

  const updateSocialLinks = (updatedLink: SocialLinkType) => {
    // setChanges((prev) => ({
    //   ...prev,
    //   updateSocialLinks: prev.updateSocialLinks.map((link) => {
    //     if (link.id === updatedLink.id) {
    //       return updatedLink;
    //     }
    //     return link;
    //   }),
    // }));

    setChangeData((prev) => ({
      ...prev,
      socialLinks: prev.socialLinks.map((link) => {
        if (link.type === updatedLink.type) {
          return updatedLink;
        }
        return link;
      }),
    }));
  };

  const updateSocialLink = (type: string, url: string) => {};

  const revertChanges = () => {
    // setChanges(emptyChanges);
    setChangeData(originalData);
  };

  const saveChanges = () => {
    // let {
    //   socialLinks: changedSocialLinks,
    //   customLinks: changedCustomLinks,
    //   ...changedProfile
    // } = changeData;
    // let { socialLinks, customLinks, ...profile } = originalData;
    // let madeChanges: ChangesType = {
    //   updateTheme: {},
    //   addCustomLinks: [],
    //   updateCustomLinks: [],
    //   deleteCustomLinks: [],
    //   addSocialLinks: [],
    //   deleteSocialLinks: [],
    //   updateSocialLinks: [],
    //   updateProfile: {},
    // };
    // if (!equal(changedProfile, profile)) {
    //   madeChanges.updateProfile = changedProfile;
    // }
    // if (!equal(changedCustomLinks, customLinks)) {
    // }
    // if (!equal(changedSocialLinks, socialLinks)) {
    //   const linkTypes = socialLinksInfo.map(
    //     (link: typeof socialLinksInfo[0]) => link.type
    //   );
    //   linkTypes.forEach((type: typeof socialLinksInfo[0]) => {
    //     let changedObj = changedSocialLinks.find((link) => link.type === type);
    //     let initialObj = socialLinks.find((link) => link.type === type);
    //     if (!equal(changedObj, initialObj)) {
    //       if (!initialObj) {
    //         // Add
    //         madeChanges.addSocialLinks.push(changedObj!);
    //       } else if (!changedObj) {
    //         // Delete
    //         madeChanges.deleteSocialLinks.push(initialObj!);
    //       } else {
    //         // Update
    //         madeChanges.updateSocialLinks.push(changedObj!);
    //       }
    //     }
    //   });
    // }
    // console.log("changes made : ", changes);

    console.log(changeData);
  };

  const makeChanges = (newData: any) => {
    setChangeData(newData);
  };

  const hasDataChanged = () => {
    return !equal(changeData, originalData);
  };

  const methods = {
    updateTheme,
    updateCustomLink,
    deleteCustomLink,
    addCustomLink,
    updateProfile,
    updateSocialLinks,
    makeChanges,
  };

  return (
    <Layout pageTitle="Admin">
      <Container
        minW="full"
        p="0"
        d="flex"
        bg="gray.100"
        style={{
          minHeight: "100vh",
        }}
      >
        {/* Desktop screen - left bar */}
        <Flex
          alignItems="center"
          justifyContent="space-between"
          flexDir="column"
          px="2"
          py="4"
          bg="white"
          minH="full"
          borderRightWidth={1}
          d={{ base: "none", md: "flex" }}
        >
          <Image src="/icon.png" alt="app logo" height={40} width={40} />

          <Menu>
            <MenuButton>
              {/* <UserIcon h={12} w={12} fill="green.400" cursor="pointer" /> */}
              <Avatar
                h={12}
                w={12}
                bg="green.500"
                src={originalData.avatar}
                rounded="full"
                objectFit="contain"
                cursor="pointer"
              />
            </MenuButton>
            <MenuList>
              <MenuItem onClick={() => signOut()}>Logout</MenuItem>
            </MenuList>
          </Menu>
        </Flex>

        <Flex flex="1" flexDir={{ base: "column", md: "row" }}>
          {/* Mobile screen - top nav bar */}
          <Flex bg="white" p="4" w="full" d={{ base: "flex", md: "none" }}>
            <Image src="/icon.png" alt="app logo" height={24} width={24} />
            <Box ml={3} as="strong">
              FollowMe
            </Box>
            <Spacer />
            <Menu>
              <MenuButton>
                <HamburgerIcon w={6} h={6} />
              </MenuButton>
              <MenuList>
                {hasDataChanged() && (
                  <MenuItem onClick={saveChanges}>Save Changes</MenuItem>
                )}
                {hasDataChanged() && (
                  <MenuItem onClick={revertChanges}>Revert Changes</MenuItem>
                )}
                <MenuItem onClick={onCopy}>Copy Profile URL</MenuItem>
                <MenuItem onClick={() => signOut()}>Logout</MenuItem>
              </MenuList>
            </Menu>
          </Flex>

          <Divider d={{ base: "block", md: "none" }} />

          {/* Links, Appearance, Settings */}
          <Box flex="1" borderRightWidth={1}>
            <Tabs defaultIndex={1}>
              <TabList bg="white" pt={1}>
                <Tab p="4">Links</Tab>
                <Tab p="4">Appearance</Tab>
                <Tab p="4">Settings</Tab>
              </TabList>

              <TabPanels>
                <TabPanel p={0}>
                  <LinksSection data={changeData} methods={methods} />
                </TabPanel>
                <TabPanel p={0}>
                  <AppearanceSection data={changeData} methods={methods} />
                </TabPanel>
                <TabPanel p={0}>
                  <SettingsSection data={changeData} methods={methods} />
                </TabPanel>
              </TabPanels>
            </Tabs>
          </Box>

          {/* Render Mobile View */}
          <Box flex="1" d={{ base: "none", md: "flex" }} flexDir="column">
            <Flex
              w="100%"
              bg="white"
              p="3.5"
              alignItems="center"
              borderBottomWidth={1}
              borderLeftWidth={1}
            >
              <Text fontSize="md">
                <chakra.strong>My FollowMe:</chakra.strong>{" "}
                <u>
                  <chakra.a href={profileURL}>{profileURL}</chakra.a>
                </u>
              </Text>
              <Spacer />
              <Button variant="outline" size="sm" onClick={onCopy}>
                {hasCopied ? "Copied!" : "Copy"}
              </Button>
            </Flex>

            <Center pos="relative" flex="1">
              <MobileView data={changeData} />

              {hasDataChanged() && (
                <Box pos="absolute" right="2" top="2">
                  <Stack pos="absolute" right="2" top="2">
                    <Button size="sm" colorScheme="green" onClick={saveChanges}>
                      Save changes
                    </Button>

                    <Button
                      size="sm"
                      colorScheme="blackAlpha"
                      onClick={revertChanges}
                    >
                      Revert changes
                    </Button>
                  </Stack>
                </Box>
              )}
            </Center>
          </Box>
        </Flex>
      </Container>
    </Layout>
  );
}

export async function getServerSideProps(context: AppContext) {
  const { req, res } = context as any;
  const session = await getSession({ req });

  console.log("GOT SESSION =====> ", session);

  // if already authenticated and session exists,
  // then show the admin page after fetching data
  if (session && res && session.username) {
    try {
      const fetchedUser = await prisma.profile.findUnique({
        where: {
          username: session.username as string,
        },
        include: {
          socialLinks: true,
          customLinks: true,
        },
      });

      let { password, email, ...user } = fetchedUser!;

      // if (session.username === user.username) {
      //   user.email = email;
      // }

      return {
        props: {
          data: user,
        },
      };
    } catch (error) {
      // return {
      //   props: {
      //     error,
      //   },
      // };
      return {
        redirect: {
          permanent: false,
          destination: "/login",
        },
      };
    }
  }

  return {
    redirect: {
      permanent: false,
      destination: "/login",
    },
  };
}

export default AdminPage;
