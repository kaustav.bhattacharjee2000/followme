import React from "react";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import axios from "axios";

import Layout from "../components/Layout";
import ProfileView from "../components/ProfileView";
import { UserInterface } from "../types/profile";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const username = context.query.profile;

  try {
    const {
      data: { data, error },
    } = await axios.get(`/api/user/get/${username}`);

    if (error) {
      return {
        redirect: {
          permanent: false,
          destination: "/404",
        },
      };
    }

    return {
      props: {
        user: data as UserInterface,
      },
    };
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  }
};

function ProfilePage({
  user,
}: InferGetServerSidePropsType<GetServerSideProps>) {
  return (
    <Layout pageTitle={`@${user.username}`}>
      <ProfileView data={user} />
    </Layout>
  );
}

export default ProfilePage;
