import React, { useState, useEffect } from "react";
import Link from "next/link";
import { AppContext } from "next/app";
import { useRouter } from "next/router";
import { signIn, getSession } from "next-auth/client";
import { Formik, Field, Form, FormikHelpers } from "formik";
import {
  Stack,
  Center,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  Text,
  Box,
  InputGroup,
  InputRightElement,
  useToast,
} from "@chakra-ui/react";
import * as Yup from "yup";

import Layout from "../components/Layout";

interface LoginFormDefault extends Record<string, string> {
  username: string;
  password: string;
}

function LoginPage() {
  const [showPassword, setShowPassword] = useState(false);
  const router = useRouter();
  const toast = useToast();

  const initialFormValues: LoginFormDefault = {
    username: "",
    password: "",
  };

  const LoginSchema = Yup.object().shape({
    username: Yup.string()
      .min(3, "Too short username!")
      .max(25, "Too long username!")
      .required("Username is required"),
    password: Yup.string()
      .min(4, "Too short password")
      .max(20, "Too long password")
      .required("Password is required"),
  });

  const handleFormSubmit = (
    values: LoginFormDefault,
    { setSubmitting }: FormikHelpers<LoginFormDefault>
  ) => {
    signIn("credentials", { ...values, redirect: false })
      .finally(() => {
        setSubmitting(false);
      })
      .then(async (response) => {
        if (response!.error) {
          toast({
            title: "Failed to login",
            description: "Check your username / password",
            status: "error",
            isClosable: true,
          });
        } else {
          await router.push("/admin");
        }
      })
      .catch((error) => {
        console.error(error);
        toast({
          title: "Failed to login",
          description: "Unknown error has occurred. Please try again",
          status: "error",
          isClosable: true,
        });
      });
  };

  const toggleShowPassword = () => setShowPassword((prev) => !prev);

  return (
    <Layout pageTitle="Login">
      <Center
        minW="full"
        bg={{ base: "white", md: "gray.100" }}
        style={{ minHeight: "100vh" }}
      >
        <Box
          w="xl"
          p={{ base: "2", md: "12" }}
          bg="white"
          boxShadow={{ base: "none", md: "md" }}
          rounded="3xl"
        >
          <Formik
            validationSchema={LoginSchema}
            initialValues={initialFormValues}
            onSubmit={handleFormSubmit}
          >
            {(props) => (
              <Form>
                <Stack spacing="4">
                  <Center>
                    <Text mb="4" fontSize="2xl">
                      Sign in to your FollowMe account
                    </Text>
                  </Center>

                  <Field name="username">
                    {({ field, form }: any) => (
                      <FormControl
                        isInvalid={
                          form.errors.username && form.touched.username
                        }
                      >
                        <FormLabel htmlFor="username">Username</FormLabel>
                        <Input
                          {...field}
                          id="username"
                          placeholder="yourusername"
                        />
                        <FormErrorMessage>
                          {form.errors.username}
                        </FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Field name="password">
                    {({ field, form }: any) => (
                      <FormControl
                        isInvalid={
                          form.errors.password && form.touched.password
                        }
                      >
                        <FormLabel htmlFor="password">Password</FormLabel>
                        <InputGroup>
                          <Input
                            {...field}
                            id="password"
                            pr="4.5rem"
                            type={showPassword ? "text" : "password"}
                            placeholder="yourpassword"
                          />
                          <InputRightElement width="4.5rem">
                            <Button
                              h="1.75rem"
                              size="sm"
                              onClick={toggleShowPassword}
                            >
                              {showPassword ? "Hide" : "Show"}
                            </Button>
                          </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>
                          {form.errors.password}
                        </FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    isDisabled={!props.dirty || !props.isValid}
                    isLoading={props.isSubmitting}
                    w="full"
                    colorScheme="purple"
                    type="submit"
                  >
                    Sign In
                  </Button>

                  <Center>
                    <Text>Forgot Password?</Text>
                  </Center>

                  <Center>
                    <Text mt="4">
                      Don't have an account?{" "}
                      <Link href="/register">
                        <Text cursor="pointer" as="strong">
                          <u>Create one</u>
                        </Text>
                      </Link>
                    </Text>
                  </Center>
                </Stack>
              </Form>
            )}
          </Formik>
        </Box>
      </Center>
    </Layout>
  );
}

export async function getServerSideProps(context: AppContext) {
  const { req, res } = context as any;
  const session = await getSession({ req });

  console.log("session info => ", session);

  // if already authenticated and session exists,
  // then redirect to admin page
  if (session && res && session.username) {
    return {
      redirect: {
        permanent: false,
        destination: "/admin",
      },
    };
  }

  return { props: {} };
}

export default LoginPage;
