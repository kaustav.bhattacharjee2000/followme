import { NextApiRequest, NextApiResponse } from "next";
import * as bcrypt from "bcrypt";

import prisma from "../../../utils/prisma";
import defaults from "../../../defaults";

import wow from "../../../data/profile.json";

const minimalUserObject = async (user: Record<string, any>) => {
  let saltRounds = parseInt(process.env.PASSWORD_SALT_ROUNDS || "10");

  console.log("hashing the password....");
  let hashedPassword = await bcrypt.hash(user.password, saltRounds);
  console.log("hashed password !");
  console.log("hash is : ", hashedPassword);

  // set defaults
  let changedUser = {
    username: user.username,
    email: user.email,
    password: hashedPassword,
    theme: defaults.themes["Gray gradient"],
    matureContent: false,
  };

  console.log("changedUser is : ", changedUser);

  return Promise.resolve(changedUser);
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { user } = req.body;

  try {
    const newUser = await minimalUserObject(user);

    const createdUser = await prisma.profile.create({
      data: newUser,
      include: {
        customLinks: true,
        socialLinks: true,
      },
    });

    console.log("CREATED USERRRRRRRRRRRRRRRRRRRR");
    console.log("HERE'S INFO --------> ", createdUser);

    let { password, ...data } = createdUser;

    return res.json({
      data,
    });
  } catch (error) {
    console.error(error);
    return res.json({
      error: {
        errorCode: 500,
        message: "Failed to create profile",
      },
    });
  }
}
