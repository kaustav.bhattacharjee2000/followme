import { NextApiRequest, NextApiResponse } from "next";

import prisma from "../../../../utils/prisma";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { username } = req.query;

  if (!username) {
    return res.json({
      error: {
        errorCode: 400,
        message: "Username not provided",
      },
    });
  }

  try {
    const fetchedUser = await prisma.profile.findUnique({
      where: {
        username: username as string,
      },
      include: {
        socialLinks: true,
        customLinks: true,
      },
    });

    let { password, ...data } = fetchedUser!;

    return res.json({
      data,
    });
  } catch (error) {
    return res.json({
      error: {
        errorCode: 404,
        message: "No such username exists",
      },
    });
  }
}
