import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import axios from "axios";
import * as bcrypt from "bcrypt";

import prisma from "../../../utils/prisma";

export default NextAuth({
  providers: [
    Providers.Credentials({
      name: "Custom provider",
      async authorize(credentials, req) {
        try {
          const pathname = new URL(req.headers.referer!).pathname.substr(1);
          console.log("request pathname => ", pathname);

          if (pathname === "login") {
            console.log("if login condition !");
            let { username, password } = Object(credentials);

            const user = await prisma.profile.findUnique({
              where: {
                username: username,
              },
            });

            console.log("HERE USER ->>>>", user);

            if (user) {
              await bcrypt.compare(password, user!.password);
              console.log("MATCHEDDDDDDDDDDDDD");
              return { username };
            }
          } else if (pathname === "register") {
            console.log("else if register condition !");
            const {
              data: { data, error },
            } = await axios.post("/api/user/create", { user: credentials });

            console.log("got data : ", data);
            console.log("got error : ", error);

            if (error) {
              return null;
            }

            return { username: data.username };
          } else {
            console.log("else nothing condition");
            return null;
          }
        } catch (error) {
          console.log("axios error happened !!!!");
          return null;
        }

        console.log("final null statement");
        return null;
      },
    }),
  ],
  session: {
    jwt: true,
  },
  jwt: {
    secret: process.env.JWT_SIGNING_PRIVATE_KEY,
    maxAge: 3 * 24 * 60 * 60, // 3 days
  },
  pages: {
    signIn: "/login",
    signOut: "/",
  },
  callbacks: {
    async jwt(token, user, account, profile, isNewUser) {
      if (user) {
        token.username = user.username;
      }
      return token;
    },
    async session(session, token) {
      // if (token.username) {
      //   const user = prisma.profile.findUnique({where: { username: token.username as string }})
      // }
      session.username = token.username;
      return session;
    },
  },
});
