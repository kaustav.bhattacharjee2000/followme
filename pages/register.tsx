import React, { useState } from "react";
import { AppContext } from "next/app";
import { useRouter } from "next/router";
import { Formik, Field, Form, FormikHelpers } from "formik";
import {
  Stack,
  Center,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  Text,
  Box,
  InputGroup,
  InputLeftAddon,
  InputRightElement,
  useToast,
} from "@chakra-ui/react";
import { signIn, getSession } from "next-auth/client";
import { CheckIcon, SmallCloseIcon } from "@chakra-ui/icons";
import Link from "next/link";
import axios from "axios";
import * as Yup from "yup";

import Layout from "../components/Layout";
import { UserInterface } from "../types/profile";

interface RegisterFormDefault extends Record<string, string> {
  username: string;
  email: string;
  password: string;
}

function RegisterPage() {
  const [showPassword, setShowPassword] = useState(false);
  const toast = useToast();
  const router = useRouter();

  const initialFormValues: RegisterFormDefault = {
    username: "",
    email: "",
    password: "",
  };

  const RegisterSchema = Yup.object().shape({
    username: Yup.string()
      .min(3, "Too short username!")
      .max(25, "Too long username!")
      .required("Username is required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    password: Yup.string()
      .min(4, "Too short password")
      .max(20, "Too long password")
      .required("Password is required"),
  });

  const toggleShowPassword = () => setShowPassword((prev) => !prev);

  const handleFormSubmit = async (
    values: RegisterFormDefault,
    { setSubmitting }: FormikHelpers<RegisterFormDefault>
  ) => {
    signIn("credentials", { ...values, redirect: false })
      .finally(() => {
        setSubmitting(false);
      })
      .then(async (response) => {
        if (response!.error) {
          console.log(response);
          toast({
            title: "Failed to register",
            description: "Account might already exist",
            status: "error",
            isClosable: true,
          });
        } else {
          await router.push("/admin");
        }
      })
      .catch((error) => {
        console.error(error);
        toast({
          title: "Failed to register",
          description: "Unknown error has occurred. Please try again",
          status: "error",
          isClosable: true,
        });
      });
  };

  return (
    <Layout pageTitle="Register">
      <Center
        minW="full"
        bg={{ base: "white", md: "gray.100" }}
        style={{ minHeight: "100vh" }}
      >
        <Box
          w="xl"
          py={{ base: "2", md: "12" }}
          px={{ base: "4", md: "12" }}
          bg="white"
          boxShadow={{ base: "none", md: "md" }}
          rounded="3xl"
        >
          <Formik
            validationSchema={RegisterSchema}
            initialValues={initialFormValues}
            onSubmit={handleFormSubmit}
          >
            {(props) => (
              <Form>
                <Stack spacing="4">
                  <Center>
                    <Text mb="4" fontSize="2xl">
                      Create a FollowMe account
                    </Text>
                  </Center>

                  <Field name="email">
                    {({ field, form }: any) => (
                      <FormControl
                        isInvalid={form.errors.email && form.touched.email}
                      >
                        <FormLabel htmlFor="email">Email</FormLabel>
                        <InputGroup>
                          <Input
                            {...field}
                            id="email"
                            placeholder="your@emailaddress.com"
                          />
                          <InputRightElement>
                            {form.errors.email && form.touched.email ? (
                              <SmallCloseIcon color="red.500" />
                            ) : (
                              form.touched.email && (
                                <CheckIcon color="green.500" />
                              )
                            )}
                          </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>{form.errors.email}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Field name="username">
                    {({ field, form }: any) => (
                      <FormControl
                        isInvalid={
                          form.errors.username && form.touched.username
                        }
                      >
                        <FormLabel htmlFor="username">Username</FormLabel>
                        <InputGroup>
                          <InputLeftAddon
                            children={`${process.env.NEXT_PUBLIC_BASE_URL}/`}
                          />
                          <Input
                            {...field}
                            id="username"
                            placeholder="yourusername"
                          />
                          <InputRightElement>
                            {form.errors.username && form.touched.username ? (
                              <SmallCloseIcon color="red.500" />
                            ) : (
                              form.touched.username && (
                                <CheckIcon color="green.500" />
                              )
                            )}
                          </InputRightElement>
                        </InputGroup>
                        <FormErrorMessage>
                          {form.errors.username}
                        </FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Field name="password">
                    {({ field, form }: any) => (
                      <FormControl
                        isInvalid={
                          form.errors.password && form.touched.password
                        }
                      >
                        <FormLabel htmlFor="password">Password</FormLabel>
                        <InputGroup>
                          <Input
                            {...field}
                            id="password"
                            pr="4.5rem"
                            type={showPassword ? "text" : "password"}
                            placeholder="yourpassword"
                          />
                          <InputRightElement width="4.5rem">
                            <Button
                              h="1.75rem"
                              size="sm"
                              onClick={toggleShowPassword}
                            >
                              {showPassword ? "Hide" : "Show"}
                            </Button>
                          </InputRightElement>
                        </InputGroup>

                        <FormErrorMessage>
                          {form.errors.password}
                        </FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    isDisabled={!props.dirty || !props.isValid}
                    isLoading={props.isSubmitting}
                    w="full"
                    colorScheme="purple"
                    type="submit"
                  >
                    Sign Up
                  </Button>

                  <Center>
                    <Text mt="4">
                      Already have an account?{" "}
                      <Link href="/login">
                        <Text cursor="pointer" as="strong">
                          <u>Sign in</u>
                        </Text>
                      </Link>
                    </Text>
                  </Center>
                </Stack>
              </Form>
            )}
          </Formik>
        </Box>
      </Center>
    </Layout>
  );
}

export async function getServerSideProps(context: AppContext) {
  const { req, res } = context as any;
  const session = await getSession({ req });

  console.log("session info => ", session);

  // if already authenticated and session exists,
  // then redirect to admin page
  if (session && res && session.username) {
    return {
      redirect: {
        permanent: false,
        destination: "/admin",
      },
    };
  }

  return { props: {} };
}

export default RegisterPage;
