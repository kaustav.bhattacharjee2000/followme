<div align="center">
  <p align="center">
    <img alt="DropNext icon" src="./public/icon.png" width="60" align="center" />
  </p>
</div>
<div align="center">
  <h1 align="center">
    FollowMe
  </h1>
</div>

A link sharing service similar to Linktree, for sharing social media links and custom links. Includes studio to edit link page and also provides link analytics.

Built with [TypeScript](https://www.typescriptlang.org/), [NextJS](https://nextjs.org/), [Chakra UI](https://chakra-ui.com/), [PostgreSQL](https://www.postgresql.org/), [Redis](https://redis.io/) and [Prisma](https://www.prisma.io/).

It's currently in progress, and version 1 will soon be completed ! Meanwhile, some screenshots are attached below for reviewing.



## Screenshots

#### Profile page

![](./screenshots/1.png)



#### Login page

![](./screenshots/2.png)



#### Register a new account


![](./screenshots/3.png)



#### Studio / Admin page (custom links section)


![](./screenshots/4.png)



#### Link analytics screen


![](./screenshots/5.png)



#### Appearance section for custom themes and buttons


![](./screenshots/6.png)



#### Settings section for profile, mature content toggle, social links


![](./screenshots/7.png)

