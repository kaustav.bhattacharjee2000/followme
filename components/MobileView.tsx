import React from "react";
import { Box } from "@chakra-ui/react";
import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";

import ProfileView from "./ProfileView";

function MobileView({ data }: any) {
  const ASPECT_RATIO = 360 / 640; // width / height
  const MOBILE_HEIGHT = 500;

  return (
    <Box
      w={ASPECT_RATIO * MOBILE_HEIGHT}
      h={MOBILE_HEIGHT}
      border="8px"
      borderRadius="40"
      borderColor="black"
      overflow="hidden"
    >
      <SimpleBar
        style={{
          maxHeight: MOBILE_HEIGHT,
          height: "100%",
        }}
      >
        <ProfileView
          data={data}
          previewHeight={MOBILE_HEIGHT - 16}
          isPreview
          scaleFactor={0.8}
        />
      </SimpleBar>
    </Box>
  );
}

export default MobileView;
