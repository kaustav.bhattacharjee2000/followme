import React from "react";
import {
  Box,
  Wrap,
  WrapItem,
  Container,
  Avatar,
  Flex,
  Center,
  Stack,
  Text,
  Icon,
} from "@chakra-ui/react";

import icons from "../utils/icon";
import { UserInterface } from "../types/profile";

interface linkProps {
  title: string;
  url: string;
  position: number;
}

interface profileViewProps {
  data: UserInterface;
  isPreview?: boolean;
  scaleFactor?: number;
  previewHeight?: number | string;
}

function ProfileView({
  data,
  isPreview,
  scaleFactor = 1,
  previewHeight,
}: profileViewProps) {
  const scale = (
    value: number,
    roundOff: boolean = true,
    nTimes: number = 1
  ): number => {
    let result = value * Math.pow(scaleFactor, nTimes);
    return roundOff ? Math.round(result) : result;
  };

  const LinkButton: React.FC<linkProps> = ({ title, url, position }) => {
    let {
      theme: {
        link: {
          backgroundColor,
          textColor,
          roundCorners,
          borderColor,
          onHover,
        },
      },
    } = data;

    return (
      <a href={url} key={`${url}-${position}`}>
        <Box
          as="button"
          p={3.5}
          w="100%"
          border={`${scale(2)}px`}
          textAlign="center"
          fontWeight="semibold"
          bg={backgroundColor}
          color={textColor}
          rounded={roundCorners}
          borderColor={borderColor}
          _hover={{
            backgroundColor: onHover!.backgroundColor,
            color: onHover!.textColor,
          }}
        >
          {title}
        </Box>
      </a>
    );
  };

  return (
    <Container
      centerContent
      flex="1"
      minW="full"
      px={scale(0)}
      py={scale(4)}
      bgImage={
        data.theme.background.coverType === "gradient"
          ? data.theme.background.gradient
          : `url('${data.theme.background.image}')`
      }
      color={data.theme.background.textColor}
      style={{
        minHeight: isPreview ? previewHeight : "100vh",
      }}
      fontSize={scale(16, true, isPreview ? 1.5 : 1)}
    >
      <Flex direction="column" align="center" mt={scale(4)} mb={scale(5)}>
        <Avatar
          bg="green.500"
          src={data.avatar}
          size={isPreview ? "lg" : "xl"}
          mb={scale(4)}
          rounded="full"
          objectFit="contain"
        />
        <Text ml={scale(3)} mr={scale(3)} fontWeight="bold" isTruncated>
          @{data.username}
        </Text>
      </Flex>

      <Container maxW="2xl">
        <Stack mt={scale(2)} spacing={scale(8)}>
          <Stack spacing={scale(4)}>
            {data.customLinks.map((eachCustomLink: any) => (
              <LinkButton {...eachCustomLink} />
            ))}
          </Stack>

          <Center>
            <Wrap spacing={isPreview ? 2 : { base: 4, md: 6, lg: 8 }}>
              {data.socialLinks.map((eachSocialLink: any) => (
                <WrapItem key={eachSocialLink.type}>
                  <Icon
                    as={icons[eachSocialLink.type]}
                    width={scale(8)}
                    height={scale(8)}
                    fill={data.theme.background.textColor}
                  />
                </WrapItem>
              ))}
            </Wrap>
          </Center>
        </Stack>
      </Container>
    </Container>
  );
}

export default ProfileView;
