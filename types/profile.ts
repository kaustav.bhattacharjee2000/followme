export type CustomLinkType = {
  id: number;
  title: string;
  position: number;
  url: string;
};

export type SocialLinkType = {
  id: number;
  type: string;
  url: string;
};

export type BackgroundThemeType = {
  textColor: string;
  coverType: "image" | "gradient";
  image?: string;
  gradient?: string;
};

export type LinkThemeType = {
  textColor: string;
  backgroundColor: string;
  roundCorners?: string;
  borderColor?: string;
  onHover?: LinkThemeType;
};

export interface ThemeInterface {
  background: BackgroundThemeType;
  link: LinkThemeType;
}

export interface UserInterface {
  id: number;
  username: string;
  avatar?: string;
  email?: string;
  customLinks: CustomLinkType[];
  socialLinks: SocialLinkType[];
  matureContent: boolean;
  theme: ThemeInterface;
}
