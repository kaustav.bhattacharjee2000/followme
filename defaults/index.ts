import backgrounds from "./backgrounds";
import buttons from "./buttons";
import themes from "./themes";

export default {
  backgrounds,
  buttons,
  themes,
};
