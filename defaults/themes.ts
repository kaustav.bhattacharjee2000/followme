const Leaf = {
  background: {
    textColor: "black",
    gradient: "linear-gradient(0deg, white, white)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "green.300",
    roundCorners: "none",
    borderColor: "green.300",
    onHover: {
      textColor: "green.300",
      backgroundColor: "transparent",
    },
  },
};

const Snow = {
  background: {
    textColor: "black",
    gradient: "linear-gradient(0deg, white, white)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "blackAlpha.800",
    roundCorners: "none",
    borderColor: "blackAlpha.800",
    onHover: {
      textColor: "blackAlpha.800",
      backgroundColor: "transparent",
    },
  },
};

const Moon = {
  background: {
    textColor: "black",
    gradient: "linear-gradient(0deg, white, white)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "blue.600",
    roundCorners: "none",
    borderColor: "blue.600",
    onHover: {
      textColor: "blue.600",
      backgroundColor: "transparent",
    },
  },
};

const Smoke = {
  background: {
    textColor: "white",
    gradient: "linear-gradient(0deg, #323336, #323336)",
    coverType: "gradient",
  },
  link: {
    textColor: "gray.700",
    backgroundColor: "white",
    roundCorners: "none",
    borderColor: "white",
    onHover: {
      textColor: "white",
      backgroundColor: "transparent",
    },
  },
};

const Noir = {
  background: {
    textColor: "white",
    gradient: "linear-gradient(0deg, #9a9b9b, #3f4049)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "transparent",
    roundCorners: "none",
    borderColor: "white",
    onHover: {
      textColor: "gray.700",
      backgroundColor: "white",
    },
  },
};

const Mint = {
  background: {
    textColor: "white",
    gradient: "linear-gradient(0deg, #c5d9cc, #68D391)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "transparent",
    roundCorners: "none",
    borderColor: "white",
    onHover: {
      textColor: "green.300",
      backgroundColor: "white",
    },
  },
};

const Wonder = {
  background: {
    textColor: "white",
    gradient:
      "linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d);",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "transparent",
    roundCorners: "none",
    borderColor: "white",
    onHover: {
      textColor: "pink",
      backgroundColor: "white",
    },
  },
};

const Golden = {
  background: {
    textColor: "white",
    gradient: "linear-gradient(319deg, #e4ff6d 0%, #ffad42 37%, #e4ff6d 100%)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "transparent",
    roundCorners: "none",
    borderColor: "white",
    onHover: {
      textColor: "pink",
      backgroundColor: "white",
    },
  },
};

const Sea = {
  background: {
    textColor: "white",
    gradient: "linear-gradient(315deg, #5de6de 0%, #b58ecc 74%)",
    coverType: "gradient",
  },
  link: {
    textColor: "white",
    backgroundColor: "transparent",
    roundCorners: "none",
    borderColor: "white",
    onHover: {
      textColor: "pink",
      backgroundColor: "white",
    },
  },
};

// const Nightly = {
//   background: {
//     textColor: "white",
//     gradient:
//       "linear-gradient(124deg, #ff2400 , #e81d1d, #e8b71d, #e3e81d, #1de840, #1ddde8, #2b1de8, #dd00f3, #dd00f3)",
//     coverType: "gradient",
//     // image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/1231630/stars.png",
//     // coverType: "image",
//   },
//   link: {
//     textColor: "white",
//     backgroundColor: "transparent",
//     roundCorners: "none",
//     borderColor: "white",
//     onHover: {
//       textColor: "pink",
//       backgroundColor: "white",
//     },
//   },
// };

// https://www.eggradients.com/gradient-color

export default {
  Leaf,
  Snow,
  Moon,
  Smoke,
  Noir,
  Mint,
  Wonder,
  Golden,
  Sea,
  // Nightly,
};
